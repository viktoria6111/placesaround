module.exports = function(grunt) {
    grunt.initConfig({

        sass: {
            dist: {
                files: {
                    './App/www/css/app.css': [
                        'src/sass/main.scss',
                        'src/sass/font-awesome.scss',
                        'src/sass/header.scss',
                        'src/sass/content-photos.scss' ,
                        'src/sass/themes.scss' ,
                        'src/sass/panels.scss'
                    ]
                }
            }
        },
        clean: {
            all: {
                src: ['./App/www']
            },
            css: {
                src: ['./App/www/css']
            },
            scripts: {
                src: ['./App/www/js']
            },
            html: {
                src: ['./App/www/**/*.html']
            }
        },
        copy: {
            html: {
                files: [
                    {
                        expand: true,
                        cwd: './src',
                        src: ['**/*.html'],
                        dest: './App/www/'
                    }
                ]
            },
            libs: {
                files: [
                    {
                        expand: true,
                        cwd: './src/libs',
                        src: ['**/*'],
                        dest: './App/www/libs'
                    }
                ]
            },
            img: {
                files: [
                    {
                        expand: true,
                        cwd: './src/img',
                        src: ['**/*'],
                        dest: './App/www/img'
                    }
                ]
            }
        },
        browserify: {
            dist: {
                files: {'./App/www/js/index.js': './src/**/main.js'}
            }
        },
        watch: {
            sass: {
                files: ['./src/**/*.scss'],
                tasks: ['clean:css', 'sass']
            },
            scripts: {
                files: ['./src/**/*.js'],
                tasks: ['clean:scripts', 'browserify']
            },
            html: {
                files: ['./src/**/*.html'],
                tasks: ['clean:html', 'copy']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['clean:all', 'sass', 'copy', 'browserify']);
};
