function InitViewFramework7(args) { //InitFramework7View

    var myApp = new Framework7();
    var mySwiper;
    var events = args.events;
    var pullToRefreshEvent = events.pullToRefreshEvent;
    var onLoadPlacesCompletedEvent = events.onLoadPlacesCompletedEvent;
    var onSlideChangeEvent = events.onSlideChangeEvent;

    // Add view
    var mainView = myApp.addView('.view-main', {
        dynamicNavbar: true,
        domCache: true,
        precompileTemplates: true,
        paginationClickable: true
    });

    // Export selectors engine
    var $$ = Dom7;

    // Build HTML using Template7 template engine
    var placeTemplateSource = $$('#places-template').html();
    var placesItemTemplate = Template7.compile(placeTemplateSource);

    var detailsTemplateSource = $$('#detales-template').html();
    var detailsTemplate = Template7.compile(detailsTemplateSource);

    var checkinsTemplateSource = $$('#checkin-template').html();
    var checkinsTemplate = Template7.compile(checkinsTemplateSource);

    var slidesTemplateSource = $$('#place-slide').html();
    var slidesTemplate = Template7.compile(slidesTemplateSource);

    var proposeCheckinSource = $$('#propose-checkin').html();
    var proposeCheckinTemplate = Template7.compile(proposeCheckinSource);

    this.buildPlacesList = function (data) {
        var renderedList = placesItemTemplate(data);
        $$('.places-list').html(renderedList);
    };

    this.showSelectedPlace = function (data) {
        var renderedList = detailsTemplate(data);
        mainView.loadContent(renderedList);

        checkinBtnListener();
    };
    
    this.showCheckIns = function (data) {
        var renderedList = checkinsTemplate(data);
        $$('#checkinContent').html(renderedList);
    };

    this.proposeChckin = function (data) {
        var renderedList = proposeCheckinTemplate(data);
        $$('#checkinContent').html(renderedList);

        checkinBtnListener();
    };
    
    this.showSlides = function (data) {
        mySwiper = myApp.swiper('.swiper-container', {
            slidesPerView: 1,
            paginationClickable: true
        });
        mySwiper.on('touchEnd', function () {
            console.log('slider moved', mySwiper.activeIndex);
        });
        var renderedList = slidesTemplate(data);
        $$('.swiper-wrapper').html(renderedList);
    };

    this.autoSwipeSlides = function (index) {
        mySwiper.slideTo(Number(index), 300);
    };

    function checkinBtnListener() {
        $$('.open-checkin').on('click', function () {
            myApp.popup('.popup-checkin');
        });
    }


    // Pull to refresh content
    var ptrContent = $$('.pull-to-refresh-content');

    // Add 'refresh' listener on it
    ptrContent.on('refresh', function () {
        pullToRefreshEvent.notify();
        // When loading done, we need to reset it
        onLoadPlacesCompletedEvent.subscribe(function () {
            myApp.pullToRefreshDone();
        });
    });
}

module.exports = InitViewFramework7;