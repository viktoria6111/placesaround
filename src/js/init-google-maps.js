  function InitMap(args) {

        var isInited = false;
        var userCoordinates = // add getters and setters
            {
                lat: args.coordinates.lat,
                lng: args.coordinates.long
            };
        var accuracy = args.coordinates.accuracy;
        var mapCenterCoordinates = args.mapCenterCoordinates;
        var events = args.events;
        var onMapLoadEvent = events.onMapLoadEvent;
        var onMapZoomChangedEvent = events.onMapZoomChangedEvent;
        var onMapCenterChangedEvent = events.onMapCenterChangedEvent;
        var onMarkersReadyEvent = events.onMarkersReadyEvent;
        var onMarkerClickedEvent = events.onMarkerClickedEvent;
        var map;
        var markerCluster;
        var mapKey = 'AIzaSyCHJAtQMBE8PH47ePtP5f1tn4b3aZGAr6k'; // вынести в инит файл или создать файл конфиг.
        var markers = [];
        var markerIcons = [];
        var self = this;

        function loadScript(src, callback) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            if(callback)script.onload = callback;
            document.getElementsByTagName("head")[0].appendChild(script);
            script.src = src;
        }

        this.initAndShowMap = function () {
            if(!isInited) {
                loadScript('https://maps.googleapis.com/maps/api/js?v=3&callback=initialize&signed_in=true&key=' + mapKey);
            } else {
                console.log('map already exist')
            }
        };

        this.dropMarkers = function (markersCoordinates) {
            userPOsitionMarker(); // add word Create
            drop(markersCoordinates);
        };

        window.initialize = function() { // перенести наверх
            isInited = true;
            var mapOptions =
            {
                center: userCoordinates,
                zoom: 16,
                disableDefaultUI: true,
                zoomControl: true
            };
            console.log(mapOptions);
            map = new google.maps.Map(document.getElementById('map'), mapOptions);

            google.maps.event.addListenerOnce(map, 'idle', function () {
                onMapLoadEvent.notify();
            });

            google.maps.event.addListener(map, 'zoom_changed', function () {
                onMapZoomChangedEvent.notify(map.getZoom());
                console.log(map.getZoom());
            });
            //center_changed

            google.maps.event.addListener(map, 'center_changed', function () {

                var center = map.getCenter();
                var coordinates = {
                    lat: center.lat(),
                    long: center.lng()
                };
                onMapCenterChangedEvent.notify(coordinates);
                onMarkersReadyEvent.subscribe(function (e, args) {
                    for(var i = 0; i < markerIcons.length; i++){
                        markerIcons[i].div.parentNode.removeChild(markerIcons[i].div);
                        markers[i].setMap(null);
                    }
                    markers = [];
                    markerIcons = [];

                    self.dropMarkers(args)
                });
                console.log(center)
            });

            var circle = new google.maps.Circle({
                center: userCoordinates,
                radius: accuracy,
                map: map, //your map,
                fillColor: '#cecece',//color,
                fillOpacity: 0.5, //opacity from 0.0 to 1.0,
                strokeColor: '#ccc', //stroke color,
                strokeOpacity: 0.5//opacity from 0.0 to 1.0
            });

            map.fitBounds(circle.getBounds());

            // Create the DIV to hold the control and call the CenterControl() constructor
            // passing in this DIV.
            var centerControlDiv = document.createElement('div');
            var centerControl = new CenterControl(centerControlDiv, map);

            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);

            /* ==========================================================================
             Custom Maker
             ========================================================================== */

            CustomMarker.prototype = new google.maps.OverlayView();

            CustomMarker.prototype.draw = function () {

                var self = this;

                var div = this.div;


                if (!div) {

                    div = this.div = document.createElement('div');

                    div.className = 'marker';

                    div.style.position = 'absolute';
                    div.style.cursor = 'pointer';
                    div.className = 'marker';
                    div.textContent = self.arguments.title;

                    if (typeof(self.arguments.marker_id) !== 'undefined') {
                        div.dataset.marker_id = self.arguments.marker_id;
                    }

                    var panes = this.getPanes();
                    panes.overlayImage.appendChild(div);
                }

                var lat = this.latlng.lat;
                var lng = this.latlng.lng;

                var latLng = new google.maps.LatLng(lat, lng);

                var point = this.getProjection().fromLatLngToDivPixel(latLng);

                if (point) {
                    div.style.left = point.x + 'px';
                    div.style.top = point.y + 'px';
                }

                function inactiveMarkers() {
                    var el = panes.overlayImage;
                    var containers = el.querySelectorAll('div')
                    for(var i = 0; i < containers.length; i++){
                        containers[i].className = 'marker';
                    }
                }

                google.maps.event.addDomListener(this.div, "click", function () {
                    inactiveMarkers();
                    onMarkerClickedEvent.notify(self.arguments.title);
                    div.className = 'clicked-marker';
                    console.log(self.arguments.title);
                });
                console.log('listener')
            };
        };


        function CustomMarker(latlng, map, arguments) {
            this.latlng = latlng;
            this.arguments = arguments;
            this.setMap(map);
        }

        function userPOsitionMarker() {
            new google.maps.Marker({
                position: userCoordinates,
                icon: 'img/1.png',
                map: map,
                zIndex: google.maps.Marker.MAX_ZINDEX + 1
            })
        }

        function drop(markersCoordinates) { // не логичное имя функции!!
            for (var i = 0; i < markersCoordinates.length; i++) {
                var counter = markersCoordinates[i].counter;
                addMarkers(markersCoordinates[i].coordinates, counter);
            }
            console.log(markersCoordinates);
        }

        function addMarkers(position, counter) { // унифицировать названия (drop || add)
            var icon = new CustomMarker(position, map, {
                title: counter,
                zIndex: 5
            });

            markerIcons.push(icon);

            markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    scaledSize : new google.maps.Size(22, 32),
                    title: '' + counter,
                })
            );
        }
        /**
         * The CenterControl adds a control to the map that recenters the map on Chicago.
         * This constructor takes the control DIV as an argument.
         * @constructor
         */
        function CenterControl(controlDiv, map) {

            // Set CSS for the control border.
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.border = '2px solid #fff';
            controlUI.style.borderRadius = '50%';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            controlUI.style.cursor = 'pointer';
            controlUI.style.marginBottom = '22px';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Click to recenter the map';
            controlDiv.appendChild(controlUI);

            // Set CSS for the control interior.
            var controlText = document.createElement('div');
            controlText.style.color = 'rgb(25,25,25)';
            controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
            controlText.style.fontSize = '16px';
            controlText.style.lineHeight = '38px';
            controlText.style.paddingLeft = '5px';
            controlText.style.paddingRight = '5px';
            controlText.innerHTML = 'CM';
            controlUI.appendChild(controlText);

            // Setup the click event listeners: simply set the map to Chicago.
            controlUI.addEventListener('click', function() {
                map.setCenter(userCoordinates);
            });

        }

    }

module.exports = InitMap;