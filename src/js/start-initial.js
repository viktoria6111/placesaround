    var InitViewFramework7 = require('./init-framework7.js'),
        LoginUser = require('./login-vk.js'), //rename
      VKGetData = require('./vk-data.js'), //rename
      Event = require('./events.js'),
        InitMap = require('./init-google-maps.js');
    
    

    var onCoordinatesObtainedEvent = new Event(),
            onLoginSuccessEvent = new Event(),
            onLoadPlacesCompletedEvent = new Event(),
            onLoadTypesCompletedEvent = new Event(),
            onLoadCheckinsCompletedEvent = new Event(),
            onLoadInfoAboutUserEvent = new Event(),
            oncheckinUserEvent = new Event(),
            onDataReadyEvent = new Event(),
            onMarkersReadyEvent = new Event(),
            onChangeDataReadyEvent = new Event(),
            onMapLoadEvent = new Event(),
            pullToRefreshEvent = new Event(),
            onMapZoomChangedEvent = new Event(),
            onMapCenterChangedEvent = new Event(),
            onMarkerClickedEvent = new Event(),
            onSlideChangeEvent = new Event();

    var framework7 = new InitViewFramework7({
        events: {
            pullToRefreshEvent: pullToRefreshEvent,
            onLoadPlacesCompletedEvent: onLoadPlacesCompletedEvent,
            onSlideChangeEvent: onSlideChangeEvent
        }
    });

    var Vk = new LoginUser({
        events: {
            onLoginSuccessEvent: onLoginSuccessEvent
        }
    });

    var data = new VKGetData({
        events: {
            onLoadPlacesCompletedEvent: onLoadPlacesCompletedEvent,
            onLoadTypesCompletedEvent: onLoadTypesCompletedEvent,
            onLoadCheckinsCompletedEvent: onLoadCheckinsCompletedEvent,
            onLoadInfoAboutUserEvent: onLoadInfoAboutUserEvent,
            oncheckinUserEvent: oncheckinUserEvent
        }
    });


    function Init() {

        var userCoordinates,
            currentItemId = 0,
            mapCenterCoordinates,
            places,
            placesTypes,
            map,
            token,
            searchRadius = 1,
            userId,
            placesCoordinates = [],
            placesData = [],
            checkIns,
            checkinUsers,
            latitude,
            longitude,
            mapButton = document.getElementById('initMap');

        this.isUserAuthorized = function () {
            Vk.checkAutorization();
        };

        this.userLogout = function() {
            Vk.logout();
        };

        pullToRefreshEvent.subscribe(function () {
            getCoordinates();
        });

        function getCoordinates () {
            navigator.geolocation.getCurrentPosition(onSuccess, onError);
        }
        
        function separatedData(data) {
            return data.response.items;
        }

        function changeDataStructure(data) {
            placesData = [];
            for(var i = 0; i < data.length; i++){
                placesData.push(
                    {
                        address: data[i].address,
                        checkins: data[i].checkins,
                        created: data[i].created,
                        distance: data[i].distance,
                        group_id: data[i].group_id,
                        icon: data[i].icon,
                        latitude: data[i].latitude,
                        longitude: data[i].longitude,
                        title: data[i].title,
                        type: data[i].type,
                        typeName: data[i].typeName,
                        updated: data[i].updated,
                        id: data[i].id
                    }
                )
            }
            onChangeDataReadyEvent.notify(placesData);
        }

        function getDataForMarkers(data) {
            placesCoordinates = [];
            
            for(var i  = 0; i < data.length; i++) {

                placesCoordinates.push(
                    {
                        coordinates: {
                            lat: data[i].latitude,
                            lng: data[i].longitude
                        },
                        image: data[i].icon,
                        counter: i
                    }
                )
            }
            onMarkersReadyEvent.notify(placesCoordinates);
            console.log(placesCoordinates);
        }

        // Show coordinates if geolocation
        function onSuccess(position) {  // дополнить имя функции
            userCoordinates =
                {
                    lat: position.coords.latitude,
                    long: position.coords.longitude,
                    accuracy: position.coords.accuracy
                };
            onCoordinatesObtainedEvent.notify(userCoordinates)
        }

        // if there is a problem getting the geolocation
        function onError(err) { // дополнить имя функции// дополнить имя функции
            console.log('We have not coordinates!', err);
        }

        function savePlacesTypes(args) {
            localStorage.setItem('places_types', JSON.stringify(args));
        }

        function getPlacesTypesName(data) {
            var types = localStorage.getItem('places_types');
            types = JSON.parse(types);

            for(var i = 0; i < data.length - 1; i++){
                
                for (var key in types.response){
                    
                    if (!types.response.hasOwnProperty(key)) continue;
                    
                    if(data[i].type == types.response[key].id){

                        data[i].typeName = types.response[key].title
                    }
                }
            }
            onDataReadyEvent.notify(places);
            console.log(places);
        }

        function addUsersInfoInCheckins() {
            var users = checkinUsers.response;//.response;
            for(var i = 0; i < checkIns.length; i++){
                for (var k = 0; k < users.length; k++){
                    if(checkIns[i].user_id == users[k].id){
                        checkIns[i].userInfo =
                        {
                            firstName: users[k].first_name,
                            lastName: users[k].last_name,
                            photo: users[k].photo_50
                        };
                        checkIns[i].date = parseDate(checkIns[i].date);
                    }
                }
            }
        }

        function parseDate(date) {
            var checkinDate = new Date(date*1000),
                day = checkinDate.getDate(),
                month = (checkinDate.getMonth() + 1),
                year = checkinDate.getFullYear(),
                hours = checkinDate.getHours(),
                minutes = checkinDate.getMinutes();
            minutes = minutes < 10 ? '0' + minutes : minutes;
            month = month < 10 ? '0' + month : month;
            day = day < 10 ? '0' + day : day;
            return day + '-' + month + '-' + year + ' at ' + hours + ':' + minutes;
        }

        /**
         * If event loginSuccesses fired
         */
        onLoginSuccessEvent.subscribe(function () {
            token = Vk.userdata.token;
            userId = Vk.userdata.id;
        });

        onLoginSuccessEvent.subscribe(function () {
            data.getPlacesTypes({
                token: token,
                userID: userId
            })
        });

        onLoginSuccessEvent.subscribe(function () {
            getCoordinates();
        });

        /**
         * If event CoordinatesObtained fired
         */
        onCoordinatesObtainedEvent.subscribe(function (e, args) {
            longitude = args.long;
            latitude = args.lat;
            data.getPlaces( //как понять из кода, что места отображаются?
                {
                    latitude: args.lat,
                    longitude: args.long,
                    radius: searchRadius,
                    token: token,
                    userID: userId
                });
        });

        onCoordinatesObtainedEvent.subscribe(function (e, args) {
            console.log(args);
            map = new InitMap({
                coordinates: userCoordinates,
                mapCenterCoordinates: mapCenterCoordinates,
                events: {
                    onMapLoadEvent: onMapLoadEvent,
                    onMapZoomChangedEvent: onMapZoomChangedEvent,
                    onMapCenterChangedEvent: onMapCenterChangedEvent,
                    onMarkersReadyEvent: onMarkersReadyEvent,
                    onMarkerClickedEvent: onMarkerClickedEvent
                }
            });
        });

        /**
         * If event LoadPlaces fired
         */
        onLoadPlacesCompletedEvent.subscribe(function (e, args) {
            places = separatedData(args);
            changeDataStructure(places);
            getPlacesTypesName(places);
            console.log(placesData);
        });

        /**
         * If event LoadCheckins fired
         */
        onLoadCheckinsCompletedEvent.subscribe(function (e, args) {
            if(args.response.items.length == 0){
                proposeCheckin(currentItemId);
            } else{
                checkIns = separatedData(args);
                getInfoAboutUsers();
                console.log(checkIns);
            }
        });

        /**
         * If event MapZoomChangedEvent fired
         */
        onMapZoomChangedEvent.subscribe(function (e, args) {
            var radius = args;
            switch (radius){
                case 4:
                    console.log(radius)
            }
        });

        /**
         * If event MapCenterChangedEvent fired
         */
        onMapCenterChangedEvent.subscribe(function (e, args) {
            mapCenterCoordinates = args;
            onCoordinatesObtainedEvent.notify(mapCenterCoordinates);
            console.log(placesData);
        });

        onChangeDataReadyEvent.subscribe(function () {
            framework7.showSlides(placesData);
            itemClickListeners('.swiper-container');
            console.log(placesData);
        });

        /**
         * If event LoadTypesCompleted fired
         */
        onLoadTypesCompletedEvent.subscribe(function (e, args) {
            placesTypes = args;
            savePlacesTypes(args)
        });

        /**
         * If event DataReady fired
         */
        onDataReadyEvent.subscribe(function (e, args) {
            framework7.buildPlacesList(args);
            itemClickListeners('.places-list');
        });

        onDataReadyEvent.subscribe(function (e, args) {
            getDataForMarkers(args); // не понятно, что происходит 
        });

        /**
         * If event InfoAboutUser fired
         */
        onLoadInfoAboutUserEvent.subscribe(function (e, args) {
            checkinUsers = args;
            addUsersInfoInCheckins();
            framework7.showCheckIns(checkIns);
            console.log(checkIns);
        });

        onMarkerClickedEvent.subscribe(function (e, args) {
            framework7.autoSwipeSlides(args);
        });

        /**
         * If event MapLoad fired
         */
        onMapLoadEvent.subscribe(function () {
            map.dropMarkers(placesCoordinates);
            framework7.showSlides(placesData);
            itemClickListeners('.swiper-container');
            console.log(placesCoordinates)
        });

        function initMap() {
            map.initAndShowMap();
            onMapLoadEvent.notify(placesCoordinates);
        }

        function getItemByIdHelper(id, obj) {
            for(var i = 0; i < obj.length; i++){
                if( obj[i].id == id){
                    framework7.showSelectedPlace(obj[i]);
                    console.log(obj[i])
                }
            }
        }

        function itemClickListeners(element) {
            var elementsList = document.querySelector(element);
            var anhors = elementsList.querySelectorAll('a.item-link');

            for (var i = 0; i < anhors.length; i++){
                anhors[i].addEventListener('click', function (index) {
                    return function(){
                        var item = anhors[index].getAttribute('data-id');
                        getItemByIdHelper(item, placesData);
                        data.getCheckins({
                            userID: userId,
                            placeID: item,
                            token: token
                        });
                        currentItemId = item;
                        console.log(item, placesData)
                    }
                }(i));
            }
        }

        function getUsersIdFromCheckins(data) {
            var ids = [];
            for(var i = 0; i < data.length; i++){
                ids.push(data[i].user_id);
            }
            console.log(ids.join(','));
            return ids.join(',');
        }

        function getInfoAboutUsers() {
            data.getInfoAboutUser(
                {
                    userID: userId,
                    userIDs: getUsersIdFromCheckins(checkIns),
                    fields: 'photo_50',
                    token: token
                }
            )
        }
        
        function proposeCheckin(id) {
            framework7.proposeChckin(id);
        }

        mapButton.addEventListener('click', initMap);
    }
    
    module.exports = Init;
