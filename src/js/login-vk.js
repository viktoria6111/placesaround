  function LoginUser(args)  {

      var self = this;

      var events = args.events;
      var loginsuccesses =  events.onLoginSuccessEvent;

      this.isToken = false; // isTokenExist
      this.userdata = {};

      this.getToken = function (){
          return window.localStorage.getItem("plugin_vk_token");
      };

      this.getUserId = function (){
          return window.localStorage.getItem("plugin_vk_user_id");
      };

      this.getExpires = function (){
          return window.localStorage.getItem("plugin_vk_exp");
      };

      this.getPerms = function (){
          return window.localStorage.getItem("plugin_vk_perms");
      };
      function deleteToken(){
          window.localStorage.removeItem("plugin_vk_token");
      }
        
      function deleteUserId(){
          window.localStorage.removeItem("plugin_vk_token");
      }
        
      function deleteExpires(){
          window.localStorage.removeItem("plugin_vk_token");
      }
        
      function deletePerms(){
          window.localStorage.removeItem("plugin_vk_token");
      }

      this.isTokenExist = function () {
          var token = self.getToken();
          if (token == undefined || token == "undefined" || token == null || token == '') {
              return false;
          }else {
              self.isToken = true;
              return true;
          }
      };

      this.checkAutorization = function() {
          if(self.isTokenExist()){
              getAuthData();//todo
              loginsuccesses.notify()
          }else {
              plugin_vk.auth();
          }
      };

      function getAuthData() {
          self.userdata.token = self.getToken();
          self.userdata.id = self.getUserId();
          self.userdata.expires = self.getExpires();
          self.userdata.perms = self.getPerms();
      }
        
      this.logout = function () {
          deleteToken();
          deleteUserId();
          deleteExpires();
          deletePerms();
          window.location.reload(true);
      };

      var url_parser = {

          get_args: function(s) {
              var tmp = [];
              s = (s.toString()).split('&');
              for (var i in s) {

                  if(!s.hasOwnProperty(i)) continue;

                  i = s[i].split("=");
                  tmp[(i[0])] = i[1];
              }
              return tmp;
          }
            /*get_args_cookie: function(s) {
                var tmp = new Array();
                s = (s.toString()).split(';');
                for (var i in s) {

                    if(!s.hasOwnProperty(i)) continue;

                    i = s[i].split("=");
                    tmp[(i[0])] = i[1];
                }
                return tmp;
            }*/
      };

      var plugin_vk = // rename variables
      {
          wwwref: false,
          plugin_perms: "friends,wall,photos,messages,wall,offline,notes", // вынести в конфиг
          client_id: 5457330,// вынести в конфиг
          auth: function (force) {
              if (!self.isToken || force) {
                  var authURL='https://oauth.vk.com/authorize?client_id=' + this.client_id + '&scope=' + this.plugin_perms + '&redirect_uri=http://oauth.vk.com/blank.html&display=touch&response_type=token'; // разбить строку TODO
                  this.wwwref = window.open(encodeURI(authURL), '_blank', 'location=no,clearsessioncache=yes,clearcache=yes,toolbar=no,disallowoverscroll=no,transitionstyle=crossdissolve'); // вынести в конфиг
                  this.wwwref.addEventListener('loadstop', this.auth_event_url);
                  this.wwwref.addEventListener('exit', self.checkAutorization);
              }
          },
          auth_event_url: function (event) {
              var tmp = (event.url).split("#");
              if (tmp[0] == 'https://oauth.vk.com/blank.html' || tmp[0]=='http://oauth.vk.com/blank.html') { // перенести в конфиг
                  tmp = url_parser.get_args(tmp[1]);
                  window.localStorage.setItem("plugin_vk_token", tmp['access_token']);
                  window.localStorage.setItem("plugin_vk_user_id", tmp['user_id']);
                  window.localStorage.setItem("plugin_vk_exp", tmp['expires_in']);
                  window.localStorage.setItem("plugin_vk_perms", plugin_vk.plugin_perms);
                  plugin_vk.wwwref.close();
              }
              console.log(window.localStorage.getItem("plugin_vk_token") );
          }
      }
  }

  module.exports = LoginUser;