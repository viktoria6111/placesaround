var Init = require('./start-initial.js');

var App = new Init();

document.addEventListener("deviceready", function () {
    
    App.isUserAuthorized();
    
    document.getElementById('logout').addEventListener('click', App.userLogout);
    
}, false); 

// TODO во всех файлах группировка функций по логике работы
