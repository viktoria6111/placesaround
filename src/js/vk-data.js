function VKGetData(args) {

    var events = args.events;
    var onLoadPlacesCompletedEvent 		= events.onLoadPlacesCompletedEvent;
    var onLoadTypesCompletedEvent 		= events.onLoadTypesCompletedEvent;
    var onLoadCheckinsCompletedEvent 	= events.onLoadCheckinsCompletedEvent;
    var onLoadInfoAboutUserEvent 			= events.onLoadInfoAboutUserEvent;
    var oncheckinUserEvent 			= events.oncheckinUserEvent;

    window.getPlacesCallback = function(result) {
        onLoadPlacesCompletedEvent.notify(result);
    };
    window.getPlacesTypesCallback = function (result) {
        onLoadTypesCompletedEvent.notify(result);
    };
    window.getCheckins = function (result) {
        onLoadCheckinsCompletedEvent.notify(result);
    };
    window.getInfoAboutUser = function (result) {
        onLoadInfoAboutUserEvent.notify(result);
    };
    window.checkinUser = function (result) {
        oncheckinUserEvent.notify(result);
    };
    //checkinUser


     function getData(method, parameters, callbackName) {

         var params = {
             language: parameters.lang ? '?lang=' + parameters.lang : '?lang=ru',
             APIVersion:parameters.version ? '&v=' + parameters.version : '&v=5.52',
             latitude: parameters.latitude ? ('&latitude=' + parameters.latitude) : '',
             longitude: parameters.longitude ? ('&longitude=' + parameters.longitude) : '',
             radius: parameters.radius ? ('&radius=' + parameters.radius) : '',
             userID: '&user_id=' + parameters.userID,
             placeID: parameters.placeID ? ('&place=' + parameters.placeID) : '',
             fields: parameters.fields ? ('&fields=' + parameters.fields) : '',
             userIDs: parameters.userIDs ? ('&user_ids=' + parameters.userIDs) : '',
             token: '&access_token=' + parameters.token
         };

         var script = document.createElement('SCRIPT');

         for(var key in params) {

             if (!params.hasOwnProperty(key)) continue;

             script.src = 'https://api.vk.com/method/'
                 + method
                 + params.language
                 + params.APIVersion
                 + params.latitude
                 + params.longitude
                 + params.radius
                 + params.userID
                 +	params.placeID
                 +	params.userIDs
                 +	params.fields
                 + params.token
                 + '&callback=' + callbackName;

         }
         document.getElementsByTagName("body")[0].appendChild(script);
    }

    /**
     * Returns a list of places that was found by the search criteria.
     * @param {object} data
     * {
     *   city: '',
     *   latitude: '',
     *   longitude: '',
     *   radius: '',
     *   offset: '',
     *   count: ''
     * }
     */

    this.getPlaces = function(data) {
        getData('places.search', data, 'getPlacesCallback');
    };

    this.getPlacesTypes = function(data) {
        getData('places.getTypes', data, 'getPlacesTypesCallback');
    };

    /**
     *
     * @param data
     * {
     * 	place: place id,
     * 	latitude: '',
     * 	longitude: '',
     * 	user_id: '',
     * 	count: '',
     * 	token: ''
     * }
     */
    this.getCheckins = function (data) {
        getData('places.getCheckins', data, 'getCheckins');
    };

    /**
     *
     * @param data
     * {
     * 	user_ids: '',
     * 	fields: (photo_id, verified, bdate, city, country, home_town, has_photo, photo_50 ...),
     * 	token: ''
     * }
     */
    this.getInfoAboutUser = function (data) {
        getData('users.get', data, 'getInfoAboutUser')
    };

    this.checkinUser = function (data) {
        //getData('places.checkin', data, 'checkinUser')
        console.log('checkin');
        console.log(data);
    }

}


module.exports = VKGetData;